package processinstance

import (
	"encoding/json"
	"errors"
	"fmt"
	"gitee.com/ha666/dingSdk/utils"
	"gitee.com/ha666/golibs"
	"net/url"
)

// 获取单个审批实例详情
func (d *ProcessInstance) Get(req *GetRequest) (res *GetResponse, err error) {
	params := url.Values{}
	params.Set("process_instance_id", req.ProcessInstanceId)
	code, body, err := utils.Get(golibs.BuildRequestUrl(utils.RequestUrl+path, params))
	if code != 200 {
		return res, errors.New(fmt.Sprintf("请求出错:%d", code))
	}
	err = json.Unmarshal(body, &res)
	if err != nil {
		return res, errors.New(fmt.Sprintf("解析结果出错:%s", err.Error()))
	}
	if res.DingtalkSmartworkBpmsProcessinstanceGetResponse.Result.DingOpenErrcode != 0 || !res.DingtalkSmartworkBpmsProcessinstanceGetResponse.Result.Success {
		return res, errors.New(fmt.Sprintf("请求出错:%d,%s", res.DingtalkSmartworkBpmsProcessinstanceGetResponse.Result.DingOpenErrcode, res.DingtalkSmartworkBpmsProcessinstanceGetResponse.Result.ErrorMsg))
	}
	return res, nil
}

type GetRequest struct {

	//审批实例id
	ProcessInstanceId string `form:"process_instance_id"`
}

type GetResponse struct {
	DingtalkSmartworkBpmsProcessinstanceGetResponse struct {
		Result struct {
			DingOpenErrcode int                 `json:"ding_open_errcode"`
			ErrorMsg        string              `json:"error_msg"`
			Success         bool                `json:"success"`
			ProcessInstance ProcessInstanceInfo `json:"process_instance"`
		} `json:"result"`
	} `json:"dingtalk_smartwork_bpms_processinstance_get_response"`
}
