package processinstance

type ProcessInstanceInfo struct {
	Title            string `json:"title"`
	CreateTime       string `json:"create_time"`
	FinishTime       string `json:"finish_time"`
	OriginatorUserid string `json:"originator_userid"`
	OriginatorDeptID string `json:"originator_dept_id"`
	Status           string `json:"status"`
	ApproverUserids  struct {
		String []string `json:"string"`
	} `json:"approver_userids"`
	CcUserids struct {
		String []string `json:"string"`
	} `json:"cc_userids"`
	FormComponentValues struct {
		FormComponentValueVo []struct {
			Name     string `json:"name"`
			Value    string `json:"value"`
			ExtValue string `json:"ext_value"`
		} `json:"form_component_value_vo"`
	} `json:"form_component_values"`
	Result           string `json:"result"`
	BusinessID       string `json:"business_id"`
	OperationRecords struct {
		OperationRecordsVo []struct {
			Userid          string `json:"userid"`
			Date            string `json:"date"`
			OperationType   string `json:"operation_type"`
			OperationResult string `json:"operation_result"`
			Remark          string `json:"remark"`
		} `json:"operation_records_vo"`
	} `json:"operation_records"`
	Tasks struct {
		TaskTopVo []struct {
			Userid     string `json:"userid"`
			TaskStatus string `json:"task_status"`
			TaskResult string `json:"task_result"`
			CreateTime string `json:"create_time"`
			FinishTime string `json:"finish_time"`
			Taskid     string `json:"taskid"`
		} `json:"task_top_vo"`
	} `json:"tasks"`
	OriginatorDeptName    string `json:"originator_dept_name"`
	BizAction             string `json:"biz_action"`
	ProcessInstanceID     string `json:"process_instance_id"`
	ProcessInstanceResult string `json:"process_instance_result"`
	//AttachedProcessInstanceIds struct {
	//	String []string `json:"string"`
	//} `json:"attached_process_instance_ids"`
}
