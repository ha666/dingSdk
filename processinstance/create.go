package processinstance

import (
	"encoding/json"
	"errors"
	"fmt"
	"gitee.com/ha666/dingSdk/utils"
	"net/url"
)

// 发起审批实例
func (d *ProcessInstance) Create(req *CreateRequest) (res *CreateResponse, err error) {
	params := url.Values{}
	params.Set("process_code", req.ProcessCode)
	params.Set("originator_user_id", req.OriginatorUserId)
	params.Set("dept_id", req.DeptId)
	params.Set("approvers", req.Approvers)
	params.Set("form_component_values", req.FormComponentValues)
	code, body, err := utils.Post(utils.RequestUrl+path+"/create", params)
	if code != 200 {
		return res, errors.New(fmt.Sprintf("请求出错:%d", code))
	}
	err = json.Unmarshal(body, &res)
	if err != nil {
		return res, errors.New(fmt.Sprintf("解析结果出错:%s", err.Error()))
	}

	if res.DingtalkSmartworkBpmsProcessinstanceCreateResponse.Result.DingOpenErrcode != 0 || !res.DingtalkSmartworkBpmsProcessinstanceCreateResponse.Result.IsSuccess {
		return res, errors.New(fmt.Sprintf("请求出错:%d,%s", res.DingtalkSmartworkBpmsProcessinstanceCreateResponse.Result.DingOpenErrcode, res.DingtalkSmartworkBpmsProcessinstanceCreateResponse.Result.ErrorMsg))
	}
	return res, nil
}

type CreateRequest struct {

	//流程表单code
	ProcessCode string `form:"process_code"`

	//发起人
	OriginatorUserId string `form:"originator_user_id"`

	//发起人部门
	DeptId string `form:"dept_id"`

	//审批人，多个用英文逗号隔开
	Approvers string `form:"approvers"`

	//表单参数
	FormComponentValues string `form:"form_component_values"`
}

type CreateResponse struct {
	DingtalkSmartworkBpmsProcessinstanceCreateResponse struct {
		Result struct {
			DingOpenErrcode   int    `json:"ding_open_errcode"`
			ErrorMsg          string `json:"error_msg"`
			IsSuccess         bool   `json:"is_success"`
			ProcessInstanceID string `json:"process_instance_id"`
		} `json:"result"`
	} `json:"dingtalk_smartwork_bpms_processinstance_create_response"`
}
