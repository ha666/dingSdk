package processinstance

import (
	"encoding/json"
	"errors"
	"fmt"
	"gitee.com/ha666/dingSdk/utils"
	"gitee.com/ha666/golibs"
	"net/url"
)

// 获取审批实例列表
func (d *ProcessInstance) List(req *ListRequest) (res *ListResponse, err error) {
	params := url.Values{}
	params.Set("process_code", req.ProcessCode)
	params.Set("start_time", req.StartTime)
	if golibs.Length(req.EndTime) > 0 {
		params.Set("end_time", req.EndTime)
	}
	if golibs.Length(req.Size) > 0 {
		params.Set("size", req.Size)
	}
	if golibs.Length(req.Cursor) > 0 {
		params.Set("cursor", req.Cursor)
	}
	if golibs.Length(req.UserIdList) > 0 {
		params.Set("userid_list", req.UserIdList)
	}
	code, body, err := utils.Get(golibs.BuildRequestUrl(utils.RequestUrl+path+"/list", params))
	if code != 200 {
		return res, errors.New(fmt.Sprintf("请求出错:%d", code))
	}
	err = json.Unmarshal(body, &res)
	if err != nil {
		return res, errors.New(fmt.Sprintf("解析结果出错:%s", err.Error()))
	}
	if res.DingtalkSmartworkBpmsProcessinstanceListResponse.Result.DingOpenErrcode != 0 || !res.DingtalkSmartworkBpmsProcessinstanceListResponse.Result.Success {
		return res, errors.New(fmt.Sprintf("请求出错:%d,%s", res.DingtalkSmartworkBpmsProcessinstanceListResponse.Result.DingOpenErrcode, res.DingtalkSmartworkBpmsProcessinstanceListResponse.Result.ErrorMsg))
	}
	return res, nil
}

type ListRequest struct {

	//流程模板唯一标识，可在 oa 后台编辑审批表单部分查询。例: PROC-FF6YR2IQO2-NP3LJ1J0SO4182NKX26K3-3N23J-PB
	ProcessCode string `form:"process_code"`

	//审批实例开始时间，毫秒级
	StartTime string `form:"start_time"`

	//审批实例结束时间，毫秒级，默认取当前值
	EndTime string `form:"end_time"`

	//分页参数，每页大小，最多传 10。默认值:10
	Size string `form:"size"`

	//分页查询的游标，最开始传 0，后续传返回参数中的 next_cursor 值
	Cursor string `form:"cursor"`

	//发起人用户 id 列表。最大列表长度:20
	UserIdList string `form:"userid_list"`
}

type ListResponse struct {
	DingtalkSmartworkBpmsProcessinstanceListResponse struct {
		Result struct {
			Result struct {
				List struct {
					ProcessInstanceTopVo []ProcessInstanceInfo `json:"process_instance_top_vo"`
				} `json:"list"`
				NextCursor int `json:"next_cursor"`
			} `json:"result"`
			DingOpenErrcode int    `json:"ding_open_errcode"`
			ErrorMsg        string `json:"error_msg"`
			Success         bool   `json:"success"`
		} `json:"result"`
	} `json:"dingtalk_smartwork_bpms_processinstance_list_response"`
}
