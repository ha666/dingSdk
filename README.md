# dingSdk

#### 项目介绍
钉钉SDK，仅支持对接服务中心

#### 使用步骤一：安装
```sh
go get gitee.com/ha666/dingSdk
```

#### 使用步骤二：创建一个客户端
```go
dingClient := dingSdk.NewDingClient(
	"https://a.b.c/dingding/",
	"12345678901234567890123456789012", 
	false)
```
参数名     |类型         |例子                                 |备注
----------|------------|------------------------------------|---------------------------
requestUrl|   string   |  https://a.b.c/dingding/           | 接口地址
authToken |   string   |  12345678901234567890123456789012  | 授权码
isDebug   |   bool     |  true                              | 是否启用调试模式，默认为否。

#### 使用步骤三：获取部门成员
```go
req := &user.SimpleListRequest{
    DepartmentId: "1",
}
res, err := dingClient.User.SimpleList(req)
if err != nil {
    fmt.Println(err.Error())
    return
}
fmt.Printf("%+v\n", res)
```