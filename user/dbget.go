package user

import (
	"encoding/json"
	"errors"
	"fmt"
	"gitee.com/ha666/dingSdk/utils"
	"gitee.com/ha666/golibs"
	"net/url"
)

// 根据用户姓名获取用户钉钉id
func (d *User) DbGet(req *DbGetRequest) (res *DbGetResponse, err error) {
	params := url.Values{}
	params.Set("name", req.Name)
	code, body, err := utils.Get(golibs.BuildRequestUrl(utils.RequestUrl+path+"/dbget", params))
	if code != 200 {
		return res, errors.New(fmt.Sprintf("请求出错:%d", code))
	}
	if len(body) <= 2 {
		return res, errors.New("未找到")
	}
	err = json.Unmarshal(body, &res)
	if err != nil {
		return res, errors.New(fmt.Sprintf("解析结果出错:%s", err.Error()))
	}
	if res.Errcode != 0 {
		return res, errors.New(fmt.Sprintf("请求出错:%d,%s", res.Errcode, res.Errmsg))
	}
	res.Errcode = 200
	return res, nil
}

type DbGetRequest struct {

	//用户姓名
	Name string `form:"name"`
}

type DbGetResponse struct {
	utils.ApiResponse
	Userid string `json:"userid"`
}
