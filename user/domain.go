package user

type SimpleUserInfo struct {
	Userid string `json:"userid"`
	Name   string `json:"name"`
}

type UserInfo struct {
	Userid     string  `json:"userid"`
	Unionid    string  `json:"unionid"`
	Mobile     string  `json:"mobile"`
	Tel        string  `json:"tel"`
	WorkPlace  string  `json:"workPlace"`
	Remark     string  `json:"remark"`
	Order      int64   `json:"order"`
	IsAdmin    bool    `json:"isAdmin"`
	IsBoss     bool    `json:"isBoss"`
	IsHide     bool    `json:"isHide"`
	IsLeader   bool    `json:"isLeader"`
	Name       string  `json:"name"`
	Active     bool    `json:"active"`
	Department []int64 `json:"department"`
	Position   string  `json:"position"`
	Email      string  `json:"email"`
	Avatar     string  `json:"avatar"`
	Jobnumber  string  `json:"jobnumber"`
}
