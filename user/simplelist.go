package user

import (
	"encoding/json"
	"errors"
	"fmt"
	"gitee.com/ha666/dingSdk/utils"
	"gitee.com/ha666/golibs"
	"net/url"
)

// 获取部门成员
func (d *User) SimpleList(req *SimpleListRequest) (res *SimpleListResponse, err error) {
	params := url.Values{}
	params.Set("department_id", req.DepartmentId)
	code, body, err := utils.Get(golibs.BuildRequestUrl(utils.RequestUrl+path+"/simplelist", params))
	if code != 200 {
		return res, errors.New(fmt.Sprintf("请求出错:%d", code))
	}
	err = json.Unmarshal(body, &res)
	if err != nil {
		return res, errors.New(fmt.Sprintf("解析结果出错:%s", err.Error()))
	}
	if res.Errcode != 0 {
		return res, errors.New(fmt.Sprintf("请求出错:%d,%s", res.Errcode, res.Errmsg))
	}
	return res, nil
}

type SimpleListRequest struct {

	//部门id
	DepartmentId string `form:"department_id"`
}

type SimpleListResponse struct {
	utils.ApiResponse
	HasMore  bool             `json:"hasMore"`
	Userlist []SimpleUserInfo `json:"userlist"`
}
