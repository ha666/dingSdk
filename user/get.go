package user

import (
	"encoding/json"
	"errors"
	"fmt"
	"gitee.com/ha666/dingSdk/utils"
	"gitee.com/ha666/golibs"
	"net/url"
)

// 获取成员详情
func (d *User) Get(req *GetRequest) (res *GetResponse, err error) {
	params := url.Values{}
	params.Set("userid", req.UserId)
	code, body, err := utils.Get(golibs.BuildRequestUrl(utils.RequestUrl+path+"/get", params))
	if code != 200 {
		return res, errors.New(fmt.Sprintf("请求出错:%d", code))
	}
	err = json.Unmarshal(body, &res)
	if err != nil {
		return res, errors.New(fmt.Sprintf("解析结果出错:%s", err.Error()))
	}
	if res.Errcode != 0 {
		return res, errors.New(fmt.Sprintf("请求出错:%d,%s", res.Errcode, res.Errmsg))
	}
	return res, nil
}

type GetRequest struct {

	//用户id
	UserId string `form:"userid"`
}

type GetResponse struct {
	utils.ApiResponse
	Unionid string `json:"unionid"`
	OpenID  string `json:"openId"`
	Roles   []struct {
		ID        int64  `json:"id"`
		Name      string `json:"name"`
		GroupName string `json:"groupName"`
	} `json:"roles"`
	Remark          string  `json:"remark"`
	Userid          string  `json:"userid"`
	IsLeaderInDepts string  `json:"isLeaderInDepts"`
	IsBoss          bool    `json:"isBoss"`
	HiredDate       int64   `json:"hiredDate"`
	IsSenior        bool    `json:"isSenior"`
	Tel             string  `json:"tel"`
	Department      []int64 `json:"department"`
	WorkPlace       string  `json:"workPlace"`
	Email           string  `json:"email"`
	OrderInDepts    string  `json:"orderInDepts"`
	Mobile          string  `json:"mobile"`
	Active          bool    `json:"active"`
	Avatar          string  `json:"avatar"`
	IsAdmin         bool    `json:"isAdmin"`
	IsHide          bool    `json:"isHide"`
	Jobnumber       string  `json:"jobnumber"`
	Name            string  `json:"name"`
	Extattr         struct {
	} `json:"extattr"`
	StateCode string `json:"stateCode"`
	Position  string `json:"position"`
}
