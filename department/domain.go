package department

type DepartmentInfo struct {
	ID              int64  `json:"id"`
	Name            string `json:"name"`
	Parentid        int64  `json:"parentid"`
	CreateDeptGroup bool   `json:"createDeptGroup"`
	AutoAddUser     bool   `json:"autoAddUser"`
}
