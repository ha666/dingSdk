package department

import (
	"encoding/json"
	"errors"
	"fmt"
	"gitee.com/ha666/dingSdk/utils"
	"gitee.com/ha666/golibs"
	"net/url"
)

// 获取部门列表
func (d *Department) ListParentDeptsByDept(req *ListParentDeptsByDeptRequest) (res *ListParentDeptsByDeptResponse, err error) {
	params := url.Values{}
	params.Set("id", req.Id)
	code, body, err := utils.Get(golibs.BuildRequestUrl(utils.RequestUrl+path+"/list_parent_depts_by_dept", params))
	if code != 200 {
		return res, errors.New(fmt.Sprintf("请求出错:%d", code))
	}
	err = json.Unmarshal(body, &res)
	if err != nil {
		return res, errors.New(fmt.Sprintf("解析结果出错:%s", err.Error()))
	}
	if res.Errcode != 0 {
		return res, errors.New(fmt.Sprintf("请求出错:%d,%s", res.Errcode, res.Errmsg))
	}
	return res, nil
}

type ListParentDeptsByDeptRequest struct {

	//部门id
	Id string `form:"id"`
}

type ListParentDeptsByDeptResponse struct {
	utils.ApiResponse
	ParentIds []int64 `json:"parentIds"`
}
