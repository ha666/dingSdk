package department

import (
	"encoding/json"
	"errors"
	"fmt"
	"gitee.com/ha666/dingSdk/utils"
	"gitee.com/ha666/golibs"
	"net/url"
)

// 获取部门列表
func (d *Department) List(req *ListRequest) (res *ListResponse, err error) {
	params := url.Values{}
	code, body, err := utils.Get(golibs.BuildRequestUrl(utils.RequestUrl+path+"/list", params))
	if code != 200 {
		return res, errors.New(fmt.Sprintf("请求出错:%d", code))
	}
	err = json.Unmarshal(body, &res)
	if err != nil {
		return res, errors.New(fmt.Sprintf("解析结果出错:%s", err.Error()))
	}
	if res.Errcode != 0 {
		return res, errors.New(fmt.Sprintf("请求出错:%d,%s", res.Errcode, res.Errmsg))
	}
	return res, nil
}

type ListRequest struct {
}

type ListResponse struct {
	utils.ApiResponse
	Department []DepartmentInfo `json:"department"`
}
