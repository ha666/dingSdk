package dingSdk

import (
	"gitee.com/ha666/dingSdk/department"
	"gitee.com/ha666/dingSdk/processinstance"
	"gitee.com/ha666/dingSdk/user"
	"gitee.com/ha666/dingSdk/utils"
)

const VERSION = "2018.1212.1540"

// 客户端
type DingClient struct {
	User            *user.User                       // 用户接口
	Department      *department.Department           // 部门接口
	ProcessInstance *processinstance.ProcessInstance // 审批接口
}

// 创建一个客户端
func NewDingClient(requestUrl, authToken string, isDebug bool) *DingClient {
	utils.RequestUrl = requestUrl
	utils.AuthToken = authToken
	utils.IsDebug = isDebug
	utils.UserAgent = "ha666_" + VERSION
	return &DingClient{
		User:            &user.User{},
		Department:      &department.Department{},
		ProcessInstance: &processinstance.ProcessInstance{},
	}
}

// 设置是否启用调试模式
func (c *DingClient) SetIsDebug(v bool) {
	utils.IsDebug = v
}

// 设置是否实时请求钉钉数据
func (c *DingClient) SetIsRealReqData(v bool) {
	utils.IsRealReqData = v
}
