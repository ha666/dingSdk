package utils

var (
	RequestUrl    = ""    // 接口地址
	AuthToken     = ""    // 授权码
	IsRealReqData = false // 是否实时请求钉钉数据
	IsDebug       = false // 是否启用调试模式
	UserAgent     = ""    // UserAgent
)
