package utils

import (
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"strings"
	"time"
)

var client *http.Client

func init() {
	client = &http.Client{
		Timeout: 15 * time.Second,
		Transport: &http.Transport{
			IdleConnTimeout: time.Duration(3) * time.Minute,
		},
	}
}

// Get请求
func Get(requestUrl string) (int, []byte, error) {
	if IsRealReqData {
		requestUrl += "&usecache=0"
	}
	reqest, err := http.NewRequest("GET", requestUrl, nil)
	if err != nil {
		return 0, nil, err
	}
	reqest.Header.Set("auth-token", AuthToken)
	reqest.Header.Set("User-Agent", UserAgent)
	response, err := client.Do(reqest)
	if err != nil {
		return 0, nil, err
	}
	defer response.Body.Close()
	if err != nil {
		return 0, nil, err
	}
	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return response.StatusCode, nil, err
	}
	if IsDebug {
		log.Printf("【get】url:%s\nresponse:%d,%+v\n", requestUrl, response.StatusCode, string(body))
	}
	return response.StatusCode, body, nil
}

// Post请求
func Post(requestUrl string, params url.Values) (int, []byte, error) {
	if IsRealReqData {
		params.Set("usecache", "0")
	}
	reqest, err := http.NewRequest("POST", requestUrl, strings.NewReader(params.Encode()))
	if err != nil {
		return 0, nil, err
	}
	reqest.Header.Set("auth-token", AuthToken)
	reqest.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	reqest.Header.Set("User-Agent", UserAgent)
	response, err := client.Do(reqest)
	if err != nil {
		return 0, nil, err
	}
	defer response.Body.Close()
	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return response.StatusCode, nil, err
	}
	if IsDebug {
		log.Printf("【post】url:%s\n,request:%+v\nresponse:%d,%+v\n", requestUrl, params, response.StatusCode, string(body))
	}
	return response.StatusCode, body, nil
}
