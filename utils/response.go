package utils

// api返回值
type ApiResponse struct {
	Errcode int    `json:"errcode"`
	Errmsg  string `json:"errmsg"`
}
